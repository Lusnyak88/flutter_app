import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'apiConstants.dart';

class ApiClient {
  static final apiKey = "i_gihudfGIUG45trgjhHUUijid5gyguuy";
  static final applicationJson  = "application/json";
  static final baseUrl_ = "jogoto.com:3000";
  static final baseUrl = "https://jogoto.com";

  // next three lines makes this class a Singleton
  static ApiClient _instance = new ApiClient.internal();
  ApiClient.internal();
  factory ApiClient() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String endPoint, {Map headers, queryParameters, encoding}) async {
//    final url = baseUrl + endPoint;
    Map<String, String> requestHeader = {
      kAuthorization : apiKey,
      kContentType : applicationJson
    };
    if(headers != null) requestHeader.addAll(headers);

    dynamic url = baseUrl + endPoint;

    if(queryParameters != null) {
      url = Uri.https(baseUrl_, endPoint, queryParameters).toString();
    }
    debugPrint(url);
    return await http.get(url, headers: requestHeader)
      .then((http.Response response) {
      final String res = response.body;
      if (json == null) { // statusCode < 200 || statusCode > 400 ||
        throw new Exception("Error while fetching data");
      }
      final obj = _decoder.convert(res);
      final data = obj[kData];
      final info = obj[kInfo];
      final error = obj[kError];
      final bool infoError = info[kError];
      if (infoError) {
        throw new Exception(error[kMessage]);
      }
      return data;
    });
  }

  Future<dynamic> post(String endPoint, {Map headers, body, encoding}) async {
    final url = baseUrl +":3000"+ endPoint;

    Map<String, String> requestHeader = {
      kAuthorization : apiKey,
      kContentType : applicationJson
    };
    if(headers != null) requestHeader.addAll(headers);

    final jsonParameters = jsonEncode(body);

    return await http.post(url, headers: requestHeader, body: jsonParameters)
    .then((http.Response response) {
      final String res = response.body;
//      final int statusCode = response.statusCode;
      if (json == null) { // statusCode < 200 || statusCode > 400 ||
        throw new Exception("Error while fetching data");
      }
      final obj = _decoder.convert(res);
      final data = obj[kData];
      final info = obj[kInfo];
      final error = obj[kError];
      final bool infoError = info[kError];
      if (infoError) {
        throw new Exception(error[kMessage]);
      }
      return data;
    });
  }
}