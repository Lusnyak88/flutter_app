import 'dart:convert';
import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';

import 'apiClient.dart';
import 'apiConstants.dart';
import '../models/user.dart';

class ApiRequestService {
  ApiClient _netUtil = new ApiClient();
  static final loginEndPoint = "/users/login";
  static final getUsersEndPoint = "/users/following/list";
  static final mobile = "mobile";
  final JsonDecoder _decoder = new JsonDecoder();

  Future<User> login(String email, String password) async {
    final params = {
      kEmail : email,
      kPassword : password,
      kDevice : mobile,
      kOS : Platform.isIOS ? 'ios' : 'android',
    };

    return await _netUtil.post(loginEndPoint, body: params)
        .then((dynamic value) {
          if(value == null) {
            throw new Exception("Empty response");
          }
          return new User.map(value);
    });
  }

  Future<Iterable<User>> getUsers(String id, String token) async {

     final queryParams = {
        "limit": "50",
        "skip": "0",
        "id": id,
     };

     final header = {
       "user_token": token,
       "u_id": id
     };

     return await _netUtil.get(getUsersEndPoint, queryParameters: queryParams, headers: header)
         .then((dynamic value){

           if(value == null) {
             throw new Exception("Empty response");
           }
           Iterable<User> users = [];
           if(value is List){
             List<dynamic> values = value;
             if(values.isNotEmpty) {
               users = values.map((e) => User.map(e));
             }
           }
           return users;
     });
  }
}