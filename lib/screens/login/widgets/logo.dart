import 'package:flutter/cupertino.dart';
import '../../../constants.dart';

class Logo extends StatelessWidget {
  final double width;

  const Logo({this.width})  : assert(width >= 0);
  
  @override
  Widget build(BuildContext context) {
    return Image(image: AssetImage(kOsCareLogoPath),
      width: width,
    );
  }
}