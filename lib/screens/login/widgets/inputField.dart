import 'package:flutter/material.dart';

import '../../../constants.dart';

class InputField extends StatelessWidget {
  final String placeholder;
  final IconData prefixIcon;
  final TextEditingController controller;
  final bool obscureText;
  final bool focusNode;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;

  const InputField({
    @required this.placeholder,
    @required this.prefixIcon,
    @required this.controller,
    this.obscureText = false,
    this.focusNode = false,
    this.validator,
    this.onSaved,
  })  : assert(placeholder != null),
        assert(prefixIcon != null),
        assert(controller != null);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: validator,
      onSaved: onSaved,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(10),
        filled: true,
        fillColor: focusNode ? kWhite : kGrey,
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          borderSide: BorderSide(
            color: Colors.red,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          borderSide: BorderSide(
            color: kGrey,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          borderSide: BorderSide(
            color: kBlue,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          borderSide: BorderSide(
            color: kGrey,
          ),
        ),
        hintText: placeholder,
        hintStyle: TextStyle(
            color: kWhite,
            fontFamily: 'Montserrat',
            fontSize: 16.0
        ),
        prefixIcon: Container(
            margin: EdgeInsets.all(1),
            color: kWhite,
            child: Icon(
              prefixIcon,
              color: kPurple,
          ),
        ),
      ),
      obscureText: obscureText,
      textAlign: TextAlign.center,
      style: TextStyle(fontFamily: 'Montserrat',
          fontSize: 16.0
      ),
    );
  }
}
