import 'package:flutter/material.dart';
import 'package:oscare/screens/profile/profile.dart';
import '../../api/apiRequestService.dart';
import '../../models/user.dart';
import '../../utils/utils.dart';
import 'widgets/inputField.dart';
import 'widgets/logo.dart';
import '../../constants.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Connection Screen',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginContainer(),
    );
  }
}

class LoginContainer extends StatefulWidget {
  @override
  _LoginContainerState createState() {
    return _LoginContainerState();
  }
}

class _LoginContainerState extends State<LoginContainer> {
  bool _isLoading = false;
  String _email, _password;
  TextEditingController _controllerEmail, _controllerPassword;

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();


  login() async {
    final form = formKey.currentState;
    if(form.validate()){
      setState(() => _isLoading = true);
      form.save();
      ApiRequestService().login(_email, _password)
          .then((value) => success(value))
          .catchError((e) => onError(e));
    }
  }

  success(User user) {
    setState(() => _isLoading = false);
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context)=>ProfileScreen(profileData: user,),
            fullscreenDialog: true
        ));
  }

  onError(Exception err) {
    setState(() => _isLoading = false);
    _controllerEmail.clear();
    _controllerPassword.clear();
    showErrorAlert(err.toString().replaceAll("Exception: ", ""), context);
  }
  
  Future<void> showErrorAlert(String message, BuildContext context){
      return showDialog(context: context,
          builder: (BuildContext context){
              return AlertDialog(title: Text( "Error"),
                content: Text(message),
                actions: [
                  FlatButton(child: Text('Ok'),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },)
              ],);
          });
  }

  @override
  initState() {
    _controllerEmail = TextEditingController();
    _controllerPassword = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controllerEmail.dispose();
    _controllerPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kPaddingL),
        child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Logo(
                    width: contextWidth(context) * 0.2
                ),
                SizedBox(height: 30.0),
               Form(
                    key: formKey,
                    child: Column(
                      children: <Widget>[
                        InputField(
                          placeholder: 'Enter your email',
                          prefixIcon: Icons.person,
                          controller: _controllerEmail,
                          obscureText: false,
                          validator: Utils().emailWarning,
                          onSaved: (value) => _email = value,
                        ),
                        SizedBox(height: 10.0),
                        InputField(
                          placeholder: 'Enter your password',
                          prefixIcon: Icons.lock,
                          controller: _controllerPassword,
                          obscureText: true,
                          validator: Utils().passwordWarning,
                          onSaved: (value) => _password = value,
                        ),
                      ],
                    ),
                ),
                SizedBox(height: 20.0),
                _isLoading ? new CircularProgressIndicator() :
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minWidth: double.infinity,
                  ),
                  child: FlatButton(
                    color: kPink,
                    padding: const EdgeInsets.all(12),
                    child: Text(
                      "Login".toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(color: kWhite),
                    ),
                    onPressed: login,
                  ),
                ),
              ],
            ),
        ),
      ),
    );
  }
}
