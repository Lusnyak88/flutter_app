import 'package:flutter/cupertino.dart';

import '../../../constants.dart';

class AvatarImage extends StatelessWidget {
  final String image;
  final double size;
  final bool isShadow;

  const AvatarImage({Key key, this.image, this.size = 60.0, this.isShadow = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            color: kGrey,
            image: DecorationImage(
                image: NetworkImage(image),
                fit: BoxFit.cover
            ),
            borderRadius: BorderRadius.all(Radius.circular(size/2)),
            boxShadow: isShadow ? [
              BoxShadow(blurRadius: 5.0, color: kBlack)
            ] : [])
    );
  }
}