import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:oscare/screens/profile/widgets/avatar.dart';
import 'package:oscare/screens/users/users.dart';
import '../../constants.dart';
import '../../models/user.dart';
import 'clipper.dart';

class ProfileScreen extends StatelessWidget {

  final User profileData;

  const ProfileScreen ({
    @required this.profileData
  }) : assert(profileData != null);

  Widget grayText(String value, BuildContext context) {
    return Text(value,
      style: Theme.of(context)
          .textTheme
          .headline6
          .copyWith(color: kDarkGrey, fontWeight: FontWeight.normal),
    );
  }

  Widget boldText(String value, BuildContext context) {
    return Text(value,
      style: Theme.of(context)
          .textTheme
          .headline5
          .copyWith(color: kBlack, fontWeight: FontWeight.bold),
    );
  }

  @override
  Widget build(BuildContext context) {
    final imageSize = contextWidth(context)*0.5;

    final avatarWidget = AvatarImage(image: profileData.image, size: imageSize);
    final fullNameTextWidget = boldText('${profileData.name} ${profileData.surname}'.toUpperCase(), context);
    final emailTextWidget = grayText(profileData.email, context);
    final dateOfBirthTextWidget = grayText(DateFormat('dd/MM/yyyy').format(profileData.dob), context);

    return new Scaffold(
        body: new Stack(
          children: <Widget>[
            ClipPath(
              child: Container(color: kPurple.withOpacity(0.8)),
              clipper: GetClipper(),
            ),
            Positioned(
                width: contextWidth(context),
                top: contextHeight(context) / 8,
                child: Column(
                  children: <Widget>[
                    avatarWidget,
                    SizedBox(height: 30.0),
                    fullNameTextWidget,
                    SizedBox(height: 10.0),
                    emailTextWidget,
                    SizedBox(height: 10.0),
                    dateOfBirthTextWidget,
                    SizedBox(height: 50.0),
                    FlatButton(
                      onPressed: (){},
                      color: kBlue,
                      padding: const EdgeInsets.all(12),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                              Text('Log out',
                                style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(color: kWhite),
                              ),
                               SizedBox(width: 10.0),
                              Icon(Icons.exit_to_app, color: kWhite,),
                           ],
                      )
                    ),
                    SizedBox(height: 15.0),
                    FlatButton(
                      color: kPink,
                      padding: const EdgeInsets.all(12),
                      child: Text(
                        "Connections",
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .copyWith(color: kWhite),
                      ),
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) => UserListScreen(
                                  id: profileData.id,
                                  token: profileData.token,
                                ),
                                fullscreenDialog: true
                            ));
                      },
                    )

                  ],
                ))
          ],
        ));
  }
}

