import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oscare/api/apiRequestService.dart';
import 'package:oscare/screens/users/widgets/userCard.dart';

import '../../constants.dart';
import '../../models/user.dart';

class UserListScreen extends StatefulWidget {
  final String id;
  final String token;

  const UserListScreen({Key key, this.id, this.token}) : super(key: key);
  
  @override
  _UserListScreenState createState() {
    return _UserListScreenState();
  }
}

class _UserListScreenState extends State<UserListScreen> {
  bool _isLoading = false;
  List<User> users = [];

  getUsers() {
    setState(() => _isLoading = true);
    if(widget.id != null && widget.token != null) {
        ApiRequestService().getUsers(widget.id, widget.token).then((value) => success(value));
    }
  }

  success(Iterable<User> userList) {
    setState(() => {
      _isLoading = false,
      users = userList.toList()
    });
  }

  Widget emptyText(){
    return Center(
      child: Text('You have no connections',
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: kBlack)
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('User List'),
      ),
      body: users.isNotEmpty ? ListView.builder(
        itemCount: users.length,
        itemBuilder: (context, index){
          return UserCard(user: users[index]);
        }) : emptyText(),
    );
  }
}





