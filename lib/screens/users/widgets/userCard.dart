import 'package:flutter/material.dart';
import 'package:oscare/screens/login/widgets/logo.dart';
import 'package:oscare/screens/profile/widgets/avatar.dart';
import '../../../models/user.dart';
import '../../../constants.dart';

class UserCard extends StatelessWidget {

  final User user;

  const UserCard({
    @required this.user
  }) : assert(user != null);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10.0),
      child: ListTile(
//        visualDensity: VisualDensity.adaptivePlatformDensity,
//        contentPadding: EdgeInsets.all(15.0),
        leading: AvatarImage(
          size: 80.0,
          image: user.image,
          isShadow: false,
        ),
        title: Text(
          '${user.name} ${user.surname}',
            style: Theme.of(context)
                .textTheme
                .headline6
                .copyWith(color: kBlack)
        ),
        subtitle: Text(user.email,
          style: Theme.of(context)
              .textTheme
              .bodyText1
              .copyWith(color: kDarkGrey),
        ),
        trailing: Icon(Icons.navigate_next,
          color: kGrey,
        ),
        onTap: (){},
      ),
    );
  }
}