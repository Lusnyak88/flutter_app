import 'package:oscare/api/apiConstants.dart';

enum Gender {
  male,
  female
}

class User {
  String _name;
  String _surname;
  String _id;
  String _image;
  String _token;
  String _email;
  Gender _gender;
  DateTime _dob;

  User.map(dynamic obj) {
    this._name = obj[kName];
    this._surname = obj[kSurname];
    this._id = obj[kId];
    this._token = obj[kToken];
    this._email = obj[kEmail];

    // date of birth
    String d = obj[kDob];
    if(d != null && d.isNotEmpty) {
        this._dob = DateTime.parse(d);
    }

    // gender
    String g = obj[kGender];
    this._gender = g == "F" ? Gender.female : Gender.male;

    // photo
    Map<String, dynamic> poster = obj[kPoster];
    if(poster != null && poster.isNotEmpty) {
      this._image = poster[kMin].toString();
    }
  }

  String get name => _name;
  String get surname => _surname;
  String get id => _id;
  String get image => _image;
  String get token => _token;
  String get email => _email;
  DateTime get dob => _dob;
  Gender get gender => _gender;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[kName] = _name;
    map[kSurname] = _surname;
    map[kDob] = _dob.toIso8601String();
    map[kPoster] = {kMin: _image};
    map[kId] = _id;
    map[kEmail] = _email;
    map[kGender] = _gender == Gender.female ? "F" : "M";
    map[kToken] = _token;

    return map;
  }
}