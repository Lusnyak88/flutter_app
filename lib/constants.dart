import 'dart:ui';
import 'package:flutter/cupertino.dart';

// Colors
const Color kGrey = Color(0xFFB7B7C1);
const Color kDarkGrey = Color(0xFF444444);
const Color kBlue = Color(0xFF84BEFE);
const Color kWhite = Color(0xFFFFFFFF);
const Color kPurple = Color(0xFF742670);
const Color kPink = Color(0xFFEC61A3);
const Color kBlack = Color(0xFF000000);

// Padding
const double kPaddingS = 8.0;
const double kPaddingM = 16.0;
const double kPaddingL = 32.0;

// Spacing
const double kSpaceS = 8.0;
const double kSpaceM = 16.0;

// Assets
const String kOsCareLogoPath = 'images/logo.png';

// Context sizes
Size contextSize(BuildContext context) => MediaQuery.of(context).size;
double contextWidth(BuildContext context) => contextSize(context).width;
double contextHeight(BuildContext context) => contextSize(context).height;
